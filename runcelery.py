from app import create_app, celery
from extensions import init_celery

app = create_app()
celery = init_celery(app, celery)
print(celery.conf['CELERY_BROKER_URL'])
