from flask_restful import fields


scene_event_schema = {
    'id': fields.Integer,
    'trigger': fields.Integer,
    'trigger_value': fields.String,
    'trigger_type': fields.String,
    'resolver': fields.Integer,
    'resolver_value': fields.String,
    'resolver_action': fields.String
}
