from app.sensors.models import Sensor
from database import db, Model, Column

ACTIONS = {
    '<': lambda x, y: x < y,
    '>': lambda x, y: x > y,
    '<=': lambda x, y: x <= y,
    '>=': lambda x, y: x >= y,
    '==': lambda x, y: x == y
}


class SceneEvent(Model):
    id = Column(db.Integer, primary_key=True)

    trigger = Column(
        db.Integer, db.ForeignKey('sensor.id'), nullable=False)
    trigger_value = Column(db.String)
    trigger_type = Column(db.String)
    resolver = Column(
        db.Integer, db.ForeignKey('sensor.id'), nullable=False)
    resolver_value = Column(db.String)
    resolver_action = Column(db.String)

    def emit(self, data, socket):
        resolver = Sensor.query.filter_by(id=self.resolver).first()
        value = data.get(self.trigger_type)
        if value:
            condition = ACTIONS.get(self.resolver_value)
            if condition:
                action = condition(int(value), int(self.trigger_value))
                if action:
                    a = getattr(resolver, self.resolver_action)
                    a(resolver.address, socket)
                    print(a)

    def parse_action(self, action):
        pass
