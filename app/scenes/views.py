from flask import Blueprint
from flask_restful import Api, Resource, marshal_with, reqparse

from app.scenes.models import SceneEvent
from app.scenes.serializers import scene_event_schema

api_v1_scenes_bp = Blueprint('api_v1_scenes_bp', __name__)

api_v1 = Api(api_v1_scenes_bp)

parser = reqparse.RequestParser()
parser.add_argument('trigger', required=True, location='json',  help='trigger can not be blank')
parser.add_argument('trigger_value', required=True, location='json',  help='trigger_value can not be blank')
parser.add_argument('trigger_type', required=True, location='json',  help='trigger_type can not be blank')
parser.add_argument('resolver', required=True, location='json',  help='resolver can not be blank')
parser.add_argument('resolver_value', location='json',  help='resolver_value can not be blank')
parser.add_argument('resolver_action', required=True, location='json',  help='resolver_action can not be blank')


class SceneEventsList(Resource):
    @marshal_with(scene_event_schema)
    def get(self):
        scene_events = SceneEvent.query.all()

        return scene_events

    @marshal_with(scene_event_schema)
    def post(self):
        args = parser.parse_args()
        scene_event = SceneEvent(
            **args

        )
        scene_event.save()

        return args, 201


api_v1.add_resource(SceneEventsList, '/events')
