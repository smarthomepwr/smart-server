import atexit
import os

from flask import Flask

from app.sensors.views import api_v1_bp
from app.rooms.views import api_room_bp

from app.sensors.listener import SensorListener
from app.settings import DevConfig
from extensions import db, migrate


def create_app(config_obj=DevConfig):
    from app.sensors.listener import SensorListener
    listener = None

    if os.environ.get("WERKZEUG_RUN_MAIN") == "true":
        listener = SensorListener()

    app = Flask(__name__)
    app.config.from_object(config_obj)

    register_extensions(app)
    register_blueprints(app)

    if listener:
        listener.start()

    def interrupt():
        global listener

    atexit.register(interrupt)
    return app


def register_extensions(app):
    with app.app_context():
        db.init_app(app)
    migrate.init_app(app, db)


def register_blueprints(app):
    from app.sensors.views import api_v1_bp
    from app.scenes.views import api_v1_scenes_bp

    app.register_blueprint(
        api_v1_bp,
        url_prefix='/api/v{version}'.format(version=1)
    )
    app.register_blueprint(
        api_room_bp,
        url_prefix='/api/v{version}'.format(version=1)
    )

    app.register_blueprint(
        api_v1_scenes_bp,
        url_prefix='/api/v{version}'.format(version=1)
    )
