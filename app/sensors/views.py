from sqlalchemy.exc import IntegrityError
from time import sleep
from flask import Blueprint, jsonify
from flask_restful import Resource, Api, reqparse, marshal_with

from app.sensors.models import Sensor
from app.sensors.serializers import sensor_schema, sensor_data_schema

api_v1_bp = Blueprint('api_v1', __name__)
api_v1 = Api(api_v1_bp)

parser = reqparse.RequestParser()
parser.add_argument('name', required=True,  help='Sensor name can not be blank')
parser.add_argument('address', required=True, help='Sensor address can not be blank')


class SensorAPI(Resource):
    @marshal_with(sensor_schema)
    def get(self, id):
        sensor = Sensor.query.filter_by(id=id).first()
        if not sensor:
            return [], 404
        return sensor, 200

    def delete(self, id):
        sensor = Sensor.query.filter_by(id=id).first()
        sensor.delete()
        return '', 204

    @marshal_with(sensor_schema)
    def put(self, id):
        args = parser.parse_args()
        sensor = Sensor.query.filter_by(id=id).first()
        try:
            sensor.update(**args)
        except IntegrityError:
            return jsonify({"error": "Sensor name and address have to be unique!"}), 400
        else:
            sensor.save()
        return sensor, 201


class SensorList(Resource):
    @marshal_with(sensor_schema)
    def get(self):
        sensors = Sensor.query.all()

        if sensors:
            return sensors
        return sensors

    @marshal_with(sensor_schema)
    def post(self):
        args = parser.parse_args()
        sensor = Sensor(name=args.get('name'), address=args.get('address'))
        sensor.save()
        return sensor, 201


class SensorFinder(Resource):

    def get(self):
        sleep(10)
        sensors = Sensor.query.all()

        return sensors


class SensorDataList(Resource):

    @marshal_with(sensor_data_schema)
    def get(self, id):
        sensor = Sensor.query.filter_by(id=id).first()
        if not sensor:
            return [], 404
        return sensor.get_data()


class SensorON(Resource):

    def put(self, id):
        sensor = Sensor.query.filter_by(id=id).first()
        if not sensor:
            return [], 404
        sensor.on()


class SensorOFF(Resource):

    def put(self, id):
        sensor = Sensor.query.filter_by(id=id).first()
        if not sensor:
            return [], 404
        sensor.off()


api_v1.add_resource(SensorAPI, '/sensors/<id>')
api_v1.add_resource(SensorDataList, '/sensors/<id>/data')
api_v1.add_resource(SensorON, '/sensors/<id>/on')
api_v1.add_resource(SensorOFF, '/sensors/<id>/off')
api_v1.add_resource(SensorList, '/sensors')
api_v1.add_resource(SensorFinder, '/sensors/find')
