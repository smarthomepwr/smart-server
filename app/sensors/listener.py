import socket
import threading
import time
from datetime import datetime
from random import choice, randint

from flask import json

from app.sensors.events import Event
from app.settings import Config

ACCEPT_SENSOR_MESSAGE = 'accepted'


class ParserOrSomething(object):
    flag = False
    obj = None

    @classmethod
    def calc(cls, obj):
        print(cls, 'flag', cls.flag)
        # lis.accept_connection = True
        if not cls.obj:
            cls.obj = obj

    @classmethod
    def change(cls):
        cls.flag = not cls.flag
        if cls.obj:
            cls.obj.accept_connection = not cls.obj.accept_connection


class SensorListener(threading.Thread):

    def __init__(self, ip='', port=12345):
        self.ip = ip
        self.port = Config.UDP_PORT
        self.udp_address = ('', self.port)
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.lock = threading.Lock()
        print('init lsuitener')
        try:
            self.sock.bind(self.udp_address)
        except:
            pass

        self.address = None
        self.data = {}
        self.accept_connection = False
        self.connected = False
        self.counter = 30
        super(SensorListener, self).__init__()

    def run(self):
        while True:
            with self.lock:
                data, self.address = self.sock.recvfrom(1024)
                print(datetime.now(), data)
                try:
                    self.data = json.loads(data.decode('utf-8'))
                except:
                    pass

                Event(self.data, self.sock, self.address)
