from flask import current_app

from app.sensors.models import Sensor
from app.scenes.models import SceneEvent


class BaseEvent(object):
    packet_type = None

    def __init__(self, data, socket, *args):
        self.data = data
        self.socket = socket
        self.sensor = None

    @classmethod
    def check_type(cls, data):
        return data.get('packet_type') == cls.packet_type

    def is_sensor_saved(self, name, address):
        from run import app
        with app.app_context():
            try:
                self.sensor = Sensor.query.filter_by(
                    name=name
                ).first()
                return self.sensor
            except:
                pass
        return None

    def check_events(self, data):
        events = SceneEvent.query.filter_by(trigger=self.sensor.id).all()
        for event in events:
            event.emit(data, self.socket)

        return events


class JoinEvent(BaseEvent):
    packet_type = 'control'
    ACCEPT_SENSOR_MESSAGE = 'accepted'.encode('utf-8')
    port = 12345  # Change to accept port from listener

    def __init__(self, data, socket, address):
        super(BaseEvent, self).__init__()
        self.socket = socket
        self.data = data
        self.address = address

        if not self.is_sensor_saved(data.get('name'), data.get('address')):
            self.save_sensor()

        self.accept_connection()

    def accept_connection(self):
        self.socket.sendto(self.ACCEPT_SENSOR_MESSAGE, self.address)

    def save_sensor(self):
        self.sensor = Sensor(
            name=self.data.get('name'),
            address=self.data.get('IP'),
            type=self.data.get('sensor_type')
        )
        self.sensor.save()


class MotionEvent(BaseEvent):
    packet_type = 'data'
    sensor_type = 'motion'

    def __init__(self, data, socket, *args):
        super(BaseEvent, self).__init__()
        self.data = data
        self.socket = socket
        self.is_sensor_saved(data.get('name'), data.get('address'))

        if self.sensor:
            self.check_events(self.data)

    @classmethod
    def check_type(cls, data):
        packet_type = data.get('packet_type')
        sensor_type = data.get('sensor_type')
        return sensor_type == cls.sensor_type and packet_type == cls.packet_type


class SensorEvent(BaseEvent):
    packet_type = 'data'
    sensor_type = 'temperature'

    def __init__(self, data, socket, *args):
        super(BaseEvent, self).__init__()
        self.data = data
        self.socket = socket

        if self.is_sensor_saved(self.data.get('name'), data.get('address')):
            self.save_data()
            self.check_events(self.data)

    def save_data(self):
        temp = self.data.get('temperature')
        hum = self.data.get('humidity')
        self.sensor.add_data(temp, hum)

    @classmethod
    def check_type(cls, data):
        packet_type = data.get('packet_type')
        sensor_type = data.get('sensor_type')
        return sensor_type == cls.sensor_type and packet_type == cls.packet_type


class Event(object):
    EVENT_TYPES = [JoinEvent, MotionEvent, SensorEvent]

    def __new__(cls, data, *args, **kwargs):
        return cls.create_event(data, args)

    @classmethod
    def create_event(cls, data, args):
        for event in cls.EVENT_TYPES:
            if event.check_type(data):
                return event(data, *args)
        return None


if __name__ == '__main__':
    sample_data = {
        "packet_type": "control",
        "sensor_type": "temperature",
        "name": "temp_hum_sensor_1",
        "IP": "192.168.1.231",
        "SSID": "Muszkieterzy"
    }
    sample_data_2 = {
        "packet_type": "data",
        "sensor_type": "temperature",
        "name": "temp_hum_sensor_2",
        "temperature": 25,
        "humidity": 61,
    }
    sample_data_3 = {
        "packet_type": "data",
        "sensor_type": "motion",
        "name": "motion_sensor",
        "motion": True,
    }
    samples = [
                sample_data,
                sample_data_2,
                sample_data_3
            ]
    for x in samples:
        print(Event(x))
    a = Event(sample_data_2)
    print(a)
    print(type(a))
