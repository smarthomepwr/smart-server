from flask_restful import fields


sensor_schema = {
    'id': fields.Integer,
    'name': fields.String,
    'type': fields.String,
    'address': fields.String,
    'added': fields.DateTime,
    'timestamp': fields.DateTime
}


sensor_data_schema = {
    'id': fields.Integer,
    'time': fields.DateTime,
    'temperature': fields.Integer,
    'humidity': fields.Integer
}

sensor_action_schema = {
    'id': fields.Integer,
    'action': fields.String
}
