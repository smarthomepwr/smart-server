import socket

from database import Model, Column, db
from app.settings import Config
from sqlalchemy.ext.declarative import declared_attr


class BaseSensor(Model):
    __abstract__ = True

    id = Column(db.Integer, primary_key=True)
    added = Column(db.DateTime,  default=db.func.current_timestamp())
    timestamp = Column(
        db.DateTime, default=db.func.current_timestamp(),
        onupdate=db.func.current_timestamp()
    )
    name = Column(db.String, unique=True, nullable=False)
    address = Column(db.String, unique=True, nullable=False)
    type = Column(db.String)

    class Meta:
        abstract = True

    # @declared_attr
    # def room(cls):
    #     return Column(Integer, ForeignKey('room.id'), nullable=True)

    # @declared_attr
    # def scene_id(cls):
    #     return Column(Integer, ForeignKey('scene.id'))

    def __init__(self, **kwargs):
        db.Model.__init__(self, **kwargs)

    def __repr__(self):
        return '<Sensor %r>' % self.name


class SensorData(Model):
    id = Column(db.Integer, primary_key=True)
    time = Column(db.DateTime,  default=db.func.current_timestamp())
    temperature = Column(db.Integer)
    humidity = Column(db.Integer)
    sensor = Column(
        db.Integer, db.ForeignKey('sensor.id'), nullable=False
    )

    def __init__(self, **kwargs):
        db.Model.__init__(self, **kwargs)


class Sensor(BaseSensor):
    data = db.relationship('SensorData', backref='sensor-d', lazy=True)

    room = Column(db.Integer, db.ForeignKey('room.id'), nullable=True)
    def add_data(self, temperature, humidity):
        from run import app
        with app.app_context():
            new_data = SensorData(
                temperature=temperature,
                humidity=humidity,
                sensor=self.id
            )
            new_data.save()

    def get_data(self):
        return SensorData.query.filter(
            SensorData.sensor == self.id
        ).all()

    def get_socket(self):
        udp_address = ('', Config.UDP_PORT)
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        try:
            sock.bind(udp_address)
        except:
            pass
        return sock

    def on(self, socket=None, ids=(1, 2)):
        if not socket:
            socket = self.get_socket()
        if self.type == "relay":
            for _id in ids:
                socket.sendto(
                    f"{_id}/on".encode('utf-8'),
                    (self.address.encode('utf-8'), Config.UDP_PORT)
                )
            print('turn on ', self.address)

    def off(self, socket=None, ids=(1, 2)):
        if not socket:
            socket = self.get_socket()
        if self.type == "relay":
            for _id in ids:
                socket.sendto(
                    f"{_id}/off".encode('utf-8'),
                    (self.address.encode('utf-8'), Config.UDP_PORT)
                )
            print('turn off ', self.address)
