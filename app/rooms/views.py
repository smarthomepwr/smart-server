from flask.json import jsonify
from flask_restful import reqparse,  Api, Resource, marshal_with
from flask import Blueprint
from sqlalchemy.exc import IntegrityError

from app.rooms.serializers import room_schema, room_with_sensors_schema
from app.rooms.models import Room
from app.sensors.models import Sensor

api_room_bp = Blueprint('room', __name__)
api_room = Api(api_room_bp)

parser = reqparse.RequestParser()

parser.add_argument('name', required=True,  help='Room name can not be blank')
parser.add_argument('description', required=False)


class RoomView(Resource):
    @marshal_with(room_with_sensors_schema)
    def get(self, id):
        room = Room.query.filter_by(id=id).first()
        sensors = Sensor.query.filter_by(room=id).all()
        room.sensors = sensors

        if not room:
            return [], 404
        return room, 200

    @marshal_with(room_schema)
    def delete(self, id):
        room = Room.query.filter_by(id=id).first()
        room.delete()
        return '', 204

    @marshal_with(room_schema)
    def put(self, id):
        args = parser.parse_args()

        room = Room.query.filter_by(id=id).first()
        try:
            room.update(**args)
        except IntegrityError:
            return jsonify({"error": "Room name has to be unique!"}), 400
        else:
            room.save()
        return room, 201


class RoomList(Resource):
    @marshal_with(room_schema)
    def get(self):
        rooms = Room.query.all()
        return rooms

    @marshal_with(room_schema)
    def post(self):
        args = parser.parse_args()
        room = Room(name=args.get('name'), description=args.get('description'))
        room.save()

        return room, 201


class RoomSensor(Resource):

    def post(self, room_id, sensor_id):
        room = Room.query.filter_by(id=room_id).first()
        sensor = Sensor.query.filter_by(id=sensor_id).first()
        if not room or not sensor:
            return [], 404
        try:
            sensor.room = room.id
        except Exception as e:
            print(e.args)
            return {'status': 'fail'}, 500
        sensor.save()
        return {'status': 'ok'}


api_room.add_resource(RoomList, '/rooms')
api_room.add_resource(RoomView, '/rooms/<id>')
api_room.add_resource(RoomSensor, '/rooms/<room_id>/addsensor/<sensor_id>')


