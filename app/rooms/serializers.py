from flask_restful import fields
from flask_restful.fields import List

from app.sensors.serializers import sensor_schema

room_schema = {
    'id': fields.Integer,
    'name': fields.String,
    'description': fields.String,
    'added': fields.DateTime,
    'timestamp': fields.DateTime
}

room_with_sensors_schema = {
    **room_schema,
    'sensors': List(fields.Nested(sensor_schema))
}
