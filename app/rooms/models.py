from database import Model, Column, db


class Room(Model):
    sensor = db.relationship('Sensor', backref='rooms', lazy=True)
    id = Column(db.Integer, primary_key=True)

    added = Column(db.DateTime,  default=db.func.current_timestamp())

    timestamp = Column(
        db.DateTime, default=db.func.current_timestamp(),
        onupdate=db.func.current_timestamp()
    )

    name = Column(db.String, unique=True, nullable=False)

    description = Column(db.String, unique=False, nullable=True)

    def __init__(self, **kwargs):
        db.Model.__init__(self, **kwargs)

    def __repr__(self):
        return '<Room %r>' % self.name
