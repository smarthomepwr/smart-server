from factory import Sequence
from factory.alchemy import SQLAlchemyModelFactory

from app.sensors.models import Sensor
from database import db


class BaseFactory(SQLAlchemyModelFactory):
    """Base factory."""

    class Meta:
        """Factory configuration."""

        abstract = True
        sqlalchemy_session = db.session


class SensorFactory(BaseFactory):
    """User factory."""
    id = Sequence(lambda n: n)
    name = Sequence(lambda n: 'sensorname{0}'.format(n+1))
    address = Sequence(lambda n: 'www.{0}.com'.format(n))

    class Meta:
        """Factory configuration."""

        model = Sensor
