from flask import url_for
import pytest

from app.sensors.models import Sensor


def test_create_sensor(testapp):
    resp = testapp.post_json(url_for('api_v1.sensorlist'), {
        "name": "sensor",
        "address": "localhost"
    })

    assert resp.json['name'] == 'sensor'
    assert resp.json['address'] == 'localhost'
    sensors = Sensor.query.all()
    assert len(sensors) == 1


def test_get_sensor_by_id(testapp, sensor):
    sensor.get()
    resp = testapp.get(url_for('api_v1.sensorapi', id=1))
    assert resp.status_code == 200
    get_sensor = Sensor.query.filter_by(id=1).first()
    assert get_sensor.id == 1


def test_update_sensor_by_id(testapp, sensor):
    old_sensor = sensor.get()
    old_name = old_sensor.name
    resp = testapp.put(url_for('api_v1.sensorapi', id=1), {
        "name": "sensor1Updated",
        "address": "localhost"
    })
    new_sensor = Sensor.query.filter_by(id=1).first()
    assert resp.status_code == 201
    assert new_sensor.name == "sensor1Updated"
    assert old_sensor.name != old_name
    assert old_sensor.name == new_sensor.name
