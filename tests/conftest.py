import pytest
from webtest import TestApp

from app import create_app
from app.sensors.models import Sensor
from config import TestConfig
from database import db as _db
from tests.factories import SensorFactory


@pytest.yield_fixture(scope='function')
def app():
    """An application for the tests."""
    _app = create_app(TestConfig)

    with _app.app_context():
        _db.create_all()

    ctx = _app.test_request_context()
    ctx.push()

    yield _app

    ctx.pop()


@pytest.fixture()
def testapp(app):
    """A Webtest app."""
    return TestApp(app)


@pytest.yield_fixture(scope='function')
def db(app):
    """A database for the tests."""
    _db.app = app
    with app.app_context():
        _db.create_all()

    yield _db

    # Explicitly close DB connection
    _db.session.close()
    _db.drop_all()


@pytest.fixture
def sensor(db):
    """A user for the tests."""
    class SensorObj():
        def get(self):
            sensor = Sensor(name="Sensor1Test", address="localhost")
            sensor.save()
            return sensor
    return SensorObj()
