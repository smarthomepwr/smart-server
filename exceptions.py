from flask import jsonify


def template(data, code=500):
    return {'message': {'errors': {'body': data}}, 'status_code': code}


SENSOR_NOT_FOUND = template(['Sensor not found'], code=404)
SENSOR_NOT_UNIQUE = template(['Sensor is not unique'], code=400)
UKNOWN_ERROR = template([], code=500)


class InvalidUsage(Exception):
    status_code = 500

    def __init__(self, message, status_code=None, payload=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_json(self):
        rv = self.message
        return jsonify(rv)

    @classmethod
    def sensor_not_found(cls):
        return cls(**SENSOR_NOT_FOUND)

    @classmethod
    def sensor_not_unique(cls):
        return cls(**SENSOR_NOT_UNIQUE)

    @classmethod
    def uknown_error(cls):
        return cls(**UKNOWN_ERROR)
